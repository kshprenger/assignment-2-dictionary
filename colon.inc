%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
                %ifdef next
                dq next  ; Сначала метка
                %else
                dq 0
                %endif 
                db %1, 0 ; Потом ключ
                %define next %2
        %else
            %error "Second arg is not an identifier!"
        %endif
    %else 
        %error "Key is not a string!"
    %endif
%endmacro
