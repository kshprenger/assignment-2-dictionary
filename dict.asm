section .text
global find_word

%include "lib.inc"

find_word:             ; Фактически rsi в начале стоит метке, указывающей на следующий элемент 
    .loop:
        push rdi
        push rsi        
        add rsi, 8     ; Сначала макрос определяет 8-ми байтную метку, следом за ней начинается 0-терминированный ключ
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .exist
        mov rsi, [rsi] ; Перемещаемся по цепочке внутри списка
        cmp rsi, 0     ; Проверка нулевой ссылки <-> конца списка
        je .not_found  ; Мы не нашли ключ
        jmp .loop
    .exist:
        add rsi, 8 ; Адрес ключа
        mov rax, rsi
        ret
    .not_found:
        xor rax, rax
        ret

