COMPILE = nasm -f elf64 -o
LD = ld -o
RM_SUPPRESS = rm -f

all: linkedList clean

linkedList: main.o dict.o lib.o
	$(LD) $@ $^

%.o: %.asm
	$(COMPILE) $@ $<

clean:
	$(RM_SUPPRESS) *.o

.PHONY:clean
