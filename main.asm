section .bss
buffer: resb 256

section .rodata
%include "words.inc"
not_found: db "Key not found", 0
too_big: db "Key is too big", 0

section .text

#define NEW_LINE_CHAR 0xA
#define BUFFER_SIZE 256

%include "lib.inc"
extern find_word

global _start

_start:
    mov rcx, 0                      ; Указатель байтов относительно адреса буфера
    .loop:
        push rcx
        call read_char              ; Читаем очередной символ в rax
        pop rcx
        cmp al, NEW_LINE_CHAR       ; Конец строки
        je .success
        cmp al, 0                   ; Конец строки (EOF)
        je .success
        mov byte[buffer + rcx], al  ; Записываем символ в буфер 
        inc rcx
        cmp rcx, BUFFER_SIZE        ; Строка оказась больше чем буфер -> ошибка
        je .fail
        jmp .loop
    .success:
        mov rdi, buffer             
        mov rsi, first
        call find_word
        cmp rax, 0              
        je .not_found
        mov rdi, rax                ; Адрес ключа
        push rdi
        call string_length          ; Считаем длину ключа в памяти (значение следует после 0-терминатора ключа)
        pop rdi                     ; Адрес ключа  
        add rdi, rax                ; Смещаемся на длину ключа
        inc rdi                     ; Значение следует после 0-терминатора ключа
        call print_string
        call print_newline
        call exit
    .fail:                      
        mov rax, 1
        mov rdi, 2
        mov rsi, too_big
        mov rdx, 14
        syscall
        call print_newline
        call exit

    .not_found:
        mov rax, 1
        mov rdi, 2
        mov rsi, not_found
        mov rdx, 13
        syscall
        call print_newline
        call exit



